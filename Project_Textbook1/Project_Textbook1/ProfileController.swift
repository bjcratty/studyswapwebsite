//
//  ProfileController.swift
//  Project_Textbook
//
//  Created by Dean Kromer on 9/19/17.
//  Copyright © 2017 Dean Kromer. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class ProfileController: UIViewController {
    
    var ref: DatabaseReference!
    
    @IBOutlet weak var isbnText: UITextField!
    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var conditionText: UITextField!
    
        
    override func viewDidLoad() {
        
        myDatabase.shared.userRef.observe(DataEventType.value, with: {
            (snapshot) in print(snapshot)
    })
    }

    @IBAction func addButton(_ sender: Any) {
        ref = Database.database().reference()
        
        let isbn = isbnText.text
        let title = titleText.text
        let condition = conditionText.text
        
        ref.child("users").child("textbooks").setValue(["isbn":isbn, "title":title, "condition": condition])
        
        
    }
    
    @IBAction func signoutButton(_ sender: Any) {       //SIGN OUT
        do {
            try Auth.auth().signOut()
            dismiss(animated: true, completion: nil)
        } catch {
            print("There was a problem logging out")
        }
    }

}
