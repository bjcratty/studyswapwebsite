//
//  SellBookController.swift
//  Project_Textbook
//
//  Created by Dean Kromer on 10/17/17.
//  Copyright © 2017 Dean Kromer. All rights reserved.
//


import UIKit
import FirebaseAuth
import FirebaseDatabase

class SellBookController: UIViewController {
    
  
    var ref: DatabaseReference?
    var myList = [User]()
    var handle:DatabaseHandle?
    
    // tableView.dataSource = self
    
    override func viewDidLoad() {
        
        ref = Database.database().reference()
        
        /* handle = ref?.child("users").observe(.childAdded, with: { (snapshot) in
         if let dict = snapshot.value as? [String: Any] {
         let userID = dict["userID"] as! String
         let email = dict["email"] as! String
         let password = dict["password"] as! String
         let user = User(userID: userID, dict: dict)
         self.myList.append(user!)
         self.tableView.reloadData()
         self.ref?.keepSynced(true)
         }
         })
         */
        //   ref?.child("users").observe(.childAdded) { (snapshot) in print(snapshot.value)}
    }
    
    @IBAction func addBook(_ sender: UIButton) {
        
        let user = Auth.auth().currentUser
        guard let uid = user?.uid else{
            return
        }
        let ref = Database.database().reference()
        let currUserRef = ref.child("users").child(uid)
        let bookRef = ref.child("textbooks").child(uid)
        
        let values = ["Title": "bookTitle", "User ID" : uid]
        
        
        //ref.child(uid).setValue(values)
        myDatabase.shared.textbookRef.childByAutoId().setValue(values)
        
        //currUserRef.updateChildValues(values)
        bookRef.updateChildValues(values)
    }
    
    
    @IBAction func signoutButton(_ sender: Any) {       //SIGN OUT
        do {
            try Auth.auth().signOut()
            dismiss(animated: true, completion: nil)
        } catch {
            print("There was a problem logging out")
        }
    }
    
}


