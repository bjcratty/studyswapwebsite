//
//  SellBooksController.swift
//  Project_Textbook
//
//  Created by Dean Kromer on 10/17/17.
//  Copyright © 2017 Dean Kromer. All rights reserved.
//

import UIKit
import Firebase

class SellBooksController: UITableViewController {

    @IBOutlet weak var isbnText: UITextField!
    @IBOutlet weak var titleText: UITextField!
    @IBOutlet weak var conditionText: UITextField!
    
    
    override func viewDidLoad() {
        
        
        
    }

    @IBAction func addButton(_ sender: UIButton) {
        
        let isbn = isbnText.text
        let title = titleText.text
        let condition = conditionText.text
        
        let user = Auth.auth().currentUser
        guard let uid = user?.uid else{
            return
        }
        let ref = Database.database().reference()
        let currUserRef = ref.child("users").child(uid)
        let bookRef = ref.child("textbooks").child(uid)
        
        let values = ["Title": "bookTitle", "User ID" : uid]
        
        ref.child("users").child("books").setValue(["isbn": isbn, "title": title, "condition": condition])
        
        
        //ref.child(uid).setValue(values)
       // myDatabase.shared.textbookRef.childByAutoId().setValue(values)
        
        //currUserRef.updateChildValues(values)
        //bookRef.updateChildValues(values)
        
    }
    

}
